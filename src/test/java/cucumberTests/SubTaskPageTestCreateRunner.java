package cucumberTests;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/createsubtask.feature",
        glue = {"utils", "createSubTaskPage"},
        dryRun = true
)
public class SubTaskPageTestCreateRunner {
}
