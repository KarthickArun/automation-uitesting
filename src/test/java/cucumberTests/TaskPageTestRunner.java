package cucumberTests;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/task.feature",
        glue = {"utils","taskPage"},
        dryRun = true)
public class TaskPageTestRunner {
}
