package cucumberTests;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/clearsubtask.feature",
        glue = {"utils", "clearSubTaskPage"},
        dryRun = true
)
public class SubTaskPageTestClearRunner {
}
