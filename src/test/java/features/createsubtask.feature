Feature: Test the task  functionality
  Scenario Outline:  : the user should able to create subtasks and not able to clear task
    Given user log in to myTask
    When user clicks my task on nav bar
    And user enters the task "SSS"
    And user click manageSubTask
    Then user able to see the subtask description and inputs <New SubTask> to the form
    And user submits with invalid <date>
    Then User click the add button
    Examples:
      | New SubTask  | date      |
      | New SubTask1 | mar122313 |
      |              | 209012334 |
