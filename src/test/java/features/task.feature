Feature: Test the task  funtionality
  Scenario Outline: the user should able to click My Tasks link on the NavBar
    Given user log in to myTask
    When user clicks my task on nav bar
    And user enters the task <Keyword>
    Then user able to see the task <Keyword> added
    And user can verify the task <Keyword> can’t have more than 250 and less than 3 characters
    Examples:
      | Keyword |
      | ##@externaldata@./src/test/resources/ExactTaskSize |
      | ##@externaldata@./src/test/resources/ExceedsTaskSize |
      | ##@externaldata@./src/test/resources/MinTaskSize |