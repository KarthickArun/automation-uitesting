Feature: Test the task  functionality
  Scenario : the user should able to create subtasks and not able to clear task
    Given user log in to myTask
    When user clicks my task on nav bar
    And user enters the task "SSS"
    And user click manageSubTask
    Then user able to see the task id
    And user should not be able to clear the task "SSS"

