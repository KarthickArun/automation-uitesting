package taskPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import loginPage.AvenueCodeLoginPageSteps;
import org.junit.Assert;

public class AvenueCodeTaskPageSteps {
    AvenueTaskPage atp=new AvenueTaskPage();
    AvenueCodeLoginPageSteps loginPage=new AvenueCodeLoginPageSteps();

    @Given("^user log in to myTask$")
    public void userLogInToMyTask() {
        loginPage.pageSignedIn();
    }

    @When("^user clicks my task on nav bar$")
    public void user_clicks_my_task_on_nav_bar() {
        atp.click(atp.getTask());
    }

    @And("^user enters the task (.+)$")
    public void user_enters_the_task(String keyword) throws Throwable {
        atp.AvenueTaskPageSubmit_file(keyword);
    }

    @Then("^user able to see the task (.+) added$")
    public void user_able_to_see_the_task_added(String keyword) throws Throwable  {
        atp.verifyText(keyword);
    }

    @And("^user can verify the task (.+) can’t have more than 250 and less than 3 characters$")
    public void user_can_verify_the_task_cant_have_more_than_250_and_less_than_3_characters(String keyword) throws Throwable {
        try {
            int actualSize = atp.numberOfCharacterInaString(keyword);
            if (actualSize < 3 && actualSize > 250)
                Assert.assertTrue("Task size is as expected", actualSize < 3 && actualSize > 250);
            else
                Assert.assertFalse("Task size is not as expected", actualSize > 3 || actualSize < 250);
        } finally {
            atp.removeATask();
        }
    }
}
