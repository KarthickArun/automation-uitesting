package taskPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.CommonUtils;

public class AvenueTaskPage extends CommonUtils {
    private final By task=By.id("my_task");
    private final By newTask=By.id("new_task");
    private final By viewTask=By.xpath("//tr[1]//td[2]//a[1]");
    private final By UserInstructions=By.linkText("User Instructions");
    private final By removeButton=By.xpath(" //button[@class='btn btn-xs btn-danger']");
    private final By removemanageSubtasksButton=By.xpath("//button[@class='btn btn-xs btn-primary ng-binding']");

    public By getRemoveButton() {
        return removeButton;
    }

    public By getRemovmanageSubtasksButton() {
        return removemanageSubtasksButton;
    }

    public By getUserInstructions() {
        return UserInstructions;
    }

    public By getNewTask() {
        return newTask;
    }

    public By getTask() {
        return task;
    }

    public void AvenueTaskPageSubmit(String letters){
        WebElement text = getElement(getNewTask());
        text.sendKeys(letters);
        text.submit();
    }

   public void verifyText(String text){
       viewTextPresent(viewTask,text);
   }

   public void removeATask(){
       click(removeButton);
   }

  public void AvenueTaskPageSubmit_file(String filepath){
      String fileToletters=readLineByLine(filepath);
      AvenueTaskPageSubmit(fileToletters);
  }
}
