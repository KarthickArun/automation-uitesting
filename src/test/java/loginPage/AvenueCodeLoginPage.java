package loginPage;

import enums.Url;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.CommonUtils;

public class AvenueCodeLoginPage extends CommonUtils {

    public static WebDriver driver;
    public WebDriverWait wait;
    private static int timeout = 10;
    private final By signIn = By.linkText("Sign In");
    private final By email = By.xpath("//input[@id='user_email']");
    private final By password = By.xpath("//input[@id='user_password']");
    private final By signInButton = By.name("commit");
    private final By QA_Assesment = By.xpath("//h1[contains(text(),'QA Assessment')]");
    private final By rememberMeCheckBox = By.id("user_remember_me");
    private final By Register = By.linkText("Register");

    public static int getTimeout() {
        return timeout;
    }

    public By getSignIn() {
        return signIn;
    }

    public By getEmail() {
        return email;
    }

    public By getPassword() {
        return password;
    }

    public By getSignInButton() {
        return signInButton;
    }

    public By getQA_Assesment() {
        return QA_Assesment;
    }

    public By getRememberMeCheckBox() {
        return rememberMeCheckBox;
    }

    public By getRegister() {
        return Register;
    }

    //Method to navigate to the URL
    public void navigateToLoginPage(){
        String url = Url.BASEURL.getURL();
        navigateToURL(url);

    }
}
