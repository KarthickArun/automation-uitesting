package loginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;

public class AvenueCodeLoginPageSteps {

    AvenueCodeLoginPage login = new AvenueCodeLoginPage();

    public void pageSignedIn() {
        login.navigateToLoginPage();
        login.click(login.getSignIn());
        login.sendKeys(login.getEmail(), "sarun_1karthick@yahoo.com");
        login.sendKeys(login.getPassword(), "Helloworld");
        login.click(login.getSignInButton());
    }

    @Given("^user is on the login page$")
    public void user_is_on_the_login_page() {
        login = new AvenueCodeLoginPage();
        login.navigateToLoginPage();
    }

    @When("user enters correct {string} and {string}")
    public void userEntersCorrectAnd(String email_id, String user_password) {
        login.click(login.getSignIn());
        login.sendKeys(login.getEmail(), email_id);
        login.sendKeys(login.getPassword(), user_password);
    }

    @Then("user sees Signed in successfully.")
    public void userSeesSignedInSuccessfully() {
        login.click(By.name("commit"));
        Assert.assertTrue(login.getElement(login.getQA_Assesment()).getText().contains("QA Assessment"));
    }
}
