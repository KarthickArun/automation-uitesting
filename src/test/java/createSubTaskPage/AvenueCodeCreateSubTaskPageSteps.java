package createSubTaskPage;

import clearSubTaskPage.AvenueCodeSubTaskPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import loginPage.AvenueCodeLoginPageSteps;
import taskPage.AvenueTaskPage;

public class AvenueCodeCreateSubTaskPageSteps {
    AvenueTaskPage atp=new AvenueTaskPage();
    AvenueCodeLoginPageSteps loginPage=new AvenueCodeLoginPageSteps();
    AvenueCodeSubTaskPage subtaskPage=new AvenueCodeSubTaskPage();


    @Given("^user log in to myTask$")
    public void user_log_in_to_mytask() throws Throwable {
        loginPage.pageSignedIn();
    }

    @When("^user clicks my task on nav bar$")
    public void user_clicks_my_task_on_nav_bar() throws Throwable {
        atp.click(atp.getTask());
    }
    @And("^user enters the task \"([^\"]*)\"$")
    public void user_enters_the_task_something(String taskName) throws Throwable {
        atp.AvenueTaskPageSubmit_file(taskName);
    }

    @And("^user click manageSubTask$")
    public void user_click_managesubtask() throws Throwable {
        subtaskPage.click(subtaskPage.getManageSubtasksButton());
        subtaskPage.currentDriverWindow();
    }

    @Then("^user able to see the subtask description and inputs (.+) to the form$")
    public void user_able_to_see_the_subtask_description_and_inputs_to_the_form(String newsubtask) throws Throwable {
        subtaskPage.AvenueSubTaskPageSubmit(newsubtask);
    }

    @And("^user submits with invalid (.+)$")
    public void user_submits_with_invalid(String date) throws Throwable {
        subtaskPage.AvenueSubTaskPageinputDate(date);
    }
    @Then("^User click the add button$")
    public void user_click_the_add_button() throws Throwable {
        subtaskPage.getAddButton();
    }
}
