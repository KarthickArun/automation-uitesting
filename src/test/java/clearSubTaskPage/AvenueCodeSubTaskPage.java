package clearSubTaskPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.CommonUtils;

/*
Sub task page locators and method specific to sub-task page are used here

 */

public class AvenueCodeSubTaskPage  extends CommonUtils {
    private final By manageSubtasksButton=By.xpath("//button[@class='btn btn-xs btn-primary ng-binding']");
    private final By editTaskID=By.xpath("//h3[@class='modal-title ng-binding']");
    private final By editTaskArea=By.xpath("//textarea[@id='edit_task']");
    private final By editSubTaskArea=By.xpath("//input[@id='new_sub_task']");
    private final By editDueDate=By.xpath("//input[@id='dueDate']");
    private final By addButton=By.xpath("//button[@id='add-subtask']");

    public By getEditTaskID() {
        return editTaskID;
    }

    public By getEditTaskArea() {
        return editTaskArea;
    }

    public By getEditSubTaskArea() {
        return editSubTaskArea;
    }

    public By getEditDueDate() {
        return editDueDate;
    }

    public By getAddButton() {
        return addButton;
    }

    public By getManageSubtasksButton() {
        return manageSubtasksButton;
    }

    //Characters are passed into the sub-task form
    public void AvenueSubTaskPageSubmit(String letters){
        WebElement text = getElement(getEditSubTaskArea());
        text.sendKeys(letters);
        text.submit();
    }

    // //Dates are passed into the sub-task form
    public void AvenueSubTaskPageinputDate(String date){
        if(isValidDate(date)) {
            WebElement text = getElement(getEditSubTaskArea());
            text.sendKeys(date);
        }
    }
}
