package clearSubTaskPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import loginPage.AvenueCodeLoginPageSteps;
import org.junit.Assert;
import taskPage.AvenueTaskPage;

public class AvenueCodeSubTaskPageSteps {
    AvenueTaskPage atp=new AvenueTaskPage();
    AvenueCodeLoginPageSteps loginPage=new AvenueCodeLoginPageSteps();
    AvenueCodeSubTaskPage subtaskPage=new AvenueCodeSubTaskPage();


    @Given("^user log in to myTask$")
    public void userLogInToMyTask() {
        loginPage.pageSignedIn();
    }

    @When("^user clicks my task on nav bar$")
    public void user_clicks_my_task_on_nav_bar() {
        atp.click(atp.getTask());
    }

    @And("^user enters the task \"([^\"]*)\"$")
    public void user_enters_the_task_something(String taskName) throws Throwable {
        atp.AvenueTaskPageSubmit_file(taskName);
    }

    @And("^user click manageSubTask $")
    public void user_click_managesubtask() throws Throwable {
        subtaskPage.click(subtaskPage.getManageSubtasksButton());
        subtaskPage.currentDriverWindow();

    }
    @Then("^user able to see the task id$")
    public void user_able_to_see_the_task_id() throws Throwable {
        String verifyifNumberIsPresent=subtaskPage.getElement(subtaskPage.getEditTaskID()).getText();
        atp.containsDigit(verifyifNumberIsPresent);
    }

    @And("^user should not be able to clear the task \"([^\"]*)\"$")
    public void user_should_not_be_able_to_clear_the_task_something(String strArg1) throws Throwable {
        Assert.assertNotNull("The object is not ready-only and the user can clear the field",
                subtaskPage.getElement(subtaskPage.getEditTaskArea()).getAttribute("readonly"));
    }
}
