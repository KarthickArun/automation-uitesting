package utils;

/*
Hooks to open and close chrome browser

 */
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverUtils {
    public  static WebDriver driver;

    @Before
    public void setUp(){
        driver =  DriverUtils.getDriver();
    }

    public static WebDriver getDriver() {
        if ( driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        }
        return driver;
    }
    @After
    public void tearDown() {
        if (driver != null) {
            driver.close();
            driver.quit();
        }
    }
}
