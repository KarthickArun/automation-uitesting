package utils;
/*
This contains all helper method which can be re-used
 */
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

public abstract class CommonUtils {
    public static WebDriver driver;
    public WebDriverWait wait;
    private static int timeout = 10;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public CommonUtils() {
        driver = DriverUtils.getDriver();
    }

    public void navigateToURL(String URL) {
        try {
            driver.navigate().to(URL);
        } catch (Exception e) {
            System.out.println("FAILURE: URL did not load: " + URL);
            throw new NotFoundException("URL did not load");
        }
    }

    public WebElement getElement(By selector) {
        try {
            return driver.findElement(selector);
        } catch (Exception e) {
            System.out.println(String.format("Element %s does not exist - proceeding", selector));
        }
        return null;
    }

    public void sendKeys(By selector, String keys) {
        try {
            driver.findElement(selector).sendKeys(keys);
        } catch (Exception e) {
            System.out.println(String.format("Element %s does not exist - proceeding", selector));
        }
    }

    public void click(By selector) {

        try {
            waitUntilElementIsDisplayedOnScreen(selector);
            driver.findElement(selector).click();
        } catch (Exception e) {
            throw new NotFoundException(String.format("The following element is not clickable: [%s]", selector));
        }
    }

    public void waitUntilElementIsDisplayedOnScreen(By selector) {
        try {
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
        } catch (Exception e) {
            throw new NotFoundException(String.format("The following element was not visible: %s ", selector));
        }
    }

    public boolean viewTextPresent(By selector, String text) {
        String actualText = driver.findElement(selector).getText();
        if (actualText.equals(text)){
            return true;
        }
        else
            return false;
    }

    public  String readLineByLine(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public int numberOfCharacterInaString(String file) {
        return readLineByLine(file).length();
    }

    public String currentDriverWindow(){
        String currentWindow=driver.getWindowHandle();
        return currentWindow;
    }

    public final boolean containsDigit(String s) {
        boolean containsDigit = false;

        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }
        return containsDigit;
    }

    public boolean isValidDate(String checkDate) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        formatter.setLenient(false);
        try {
            Date date= formatter.parse(checkDate);
            System.out.println("User has provided an valid date: " +date);
            return true;
        } catch (ParseException e) {
           System.out.println("User has provided an invalid date");
           return false;
        }
    }
}
