package enums;
/*
URL is defined in this method
 */


public enum Url {
    BASEURL("https://qa-test.avenuecode.com/");
    String url;

    Url(String url){
        this.url = url;
    }

    public String getURL() {
        return url;
    }
}
